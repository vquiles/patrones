package es.talent.patrone.adapter2;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Ordenador {
	private SD ranuraSD;
	
	public void insertarTarjetaEnRanura(SD tarjeta) {
		this.ranuraSD = tarjeta;
	}
	
	public void leerDatosSD() {
		System.out.println(this.ranuraSD.transferirInformacion());
	}
}
