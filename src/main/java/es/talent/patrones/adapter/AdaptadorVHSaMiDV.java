package es.talent.patrones.adapter;

public class AdaptadorVHSaMiDV implements MiDV {

	private VHS cintaVHS;
	
	public String muestraFrame1() {
		return this.cintaVHS.getFrames().get(0);
	}

	public String muestraFrame2() {
		return this.cintaVHS.getFrames().get(1);
	}

	public String muestraFrame3() {
		return this.cintaVHS.getFrames().get(2);
	}

	public String muestraFrame4() {
		return this.cintaVHS.getFrames().get(3);
	}

	public String muestraFrame5() {
		return this.cintaVHS.getFrames().get(4);
	}

}
