package es.talent.patrones.adapter;

public class Reproductor {
	private VHS cinta;

	public void reporducirCinta() {
		while (!this.cinta.fin()) {
			System.out.println(this.cinta.siguienteFrame());
		}
	}

	public void introducirCinta(VHS nuevaCinta) {
		this.cinta = nuevaCinta;

	}
}
