package es.talent.patrones.adapter;

public class CintaMiDV implements MiDV {
	private String frame1 = "Contenido f1 midv";
	private String frame2 = "Contenido f2 midv";
	private String frame3 = "Contenido f3 midv";
	private String frame4 = "Contenido f4 midv";
	private String frame5 = "Contenido f5 midv";

	public String muestraFrame1() {
		return this.frame1;
	}

	public String muestraFrame2() {
		return this.frame2;
	}

	public String muestraFrame3() {
		return this.frame3;
	}

	public String muestraFrame4() {
		return this.frame4;
	}

	public String muestraFrame5() {
		return this.frame5;
	}

}
