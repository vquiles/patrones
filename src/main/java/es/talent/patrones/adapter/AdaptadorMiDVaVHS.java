package es.talent.patrones.adapter;

import java.util.List;

public class AdaptadorMiDVaVHS implements VHS {

	private MiDV midv;
	private int contador;

	public AdaptadorMiDVaVHS() {
		this.contador = 0;
	}
	
	public void insertarMiDVenAdaptador(MiDV cinta) {
		this.midv = cinta;
	}

	public String siguienteFrame() {
		String salida = null;
		switch (this.contador) {
		case 0:
			salida = this.midv.muestraFrame1();
			break;
		case 1:
			salida = this.midv.muestraFrame2();
			break;
		case 2:
			salida = this.midv.muestraFrame3();
			break;
		case 3:
			salida = this.midv.muestraFrame4();
			break;
		case 4:
			salida = this.midv.muestraFrame5();
			break;
		}
		this.contador++;
		return salida;
	}

	public boolean fin() {
		return this.contador >= 5;
	}

	public List<String> getFrames() {
		return null;
	}

}
