package es.talent.patrones.adapter;

import java.util.List;

public interface VHS {
	public String siguienteFrame();
	public boolean fin();
	public List<String> getFrames();
}
