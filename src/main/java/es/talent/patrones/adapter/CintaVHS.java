package es.talent.patrones.adapter;

import java.util.ArrayList;
import java.util.List;

public class CintaVHS implements VHS {
	
	private List<String> frames;
	private int cont;
	
	public CintaVHS() {
		this.frames = new ArrayList<String>();
		this.frames.add("Contenido del frame 1");
		this.frames.add("Contenido del frame 2");
		this.frames.add("Contenido del frame 3");
		this.frames.add("Contenido del frame 4");
		this.frames.add("Contenido del frame 5");
		this.cont = 0;
	}

	public String siguienteFrame() {
		String salida = null;
		
		if (this.cont < 5) {
			salida = this.frames.get(this.cont);
			this.cont++;
		}
		
		return salida;
	}

	public boolean fin() {
		return this.cont >= 5;
	}

	public List<String> getFrames() {
		return this.frames;
	}

}
