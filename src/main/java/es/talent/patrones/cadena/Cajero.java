package es.talent.patrones.cadena;

public class Cajero {
	private CajeroHandler billetera50;
	private CajeroHandler billetera20;
	private CajeroHandler billetera10;
	private CajeroHandler billetera1;
	
	public Cajero() {
		this.billetera1 = new CajeroHandler(1, null);
		this.billetera10 = new CajeroHandler(10, this.billetera1);
		this.billetera20 = new CajeroHandler(20, this.billetera10);
		this.billetera50 = new CajeroHandler(50, this.billetera20);
	}
	
	public void sacarDinero(int cantidad) {
		this.billetera50.devolverBilletes(cantidad);
	}

}
