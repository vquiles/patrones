package es.talent.patrones.cadena;

public class CajeroHandler {

	private int tamanyoBillete;
	private CajeroHandler siguienteHandler;

	public CajeroHandler(int tamanyo, CajeroHandler siguiente) {
		this.tamanyoBillete = tamanyo;
		this.siguienteHandler = siguiente;
	}

	public void devolverBilletes(int cantidad) {
		while (cantidad >= this.tamanyoBillete) {
			System.out.println("Suelto billete de: " + this.tamanyoBillete);
			cantidad = cantidad - this.tamanyoBillete;
		}
		if (cantidad > 0) {
			this.siguienteHandler.devolverBilletes(cantidad);
		}
	}
}
